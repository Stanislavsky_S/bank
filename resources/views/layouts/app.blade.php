<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href={{asset('css/app.css')}} rel="stylesheet">
    <script src={{asset('js/app.js')}}></script>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top header-main">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed  header_mobile_menu-btn" data-toggle="collapse" data-target="#app-navbar-collapse">

                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar custom__icon"></span>
                <span class="icon-bar custom__icon"></span>
                <span class="icon-bar custom__icon"></span>
            </button>

        </div>
        <a class="navbar-brand" href="{{route('main')}}">Главная</a>


        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{route('main')}}">Банк</a></li>
                <li><a href="{{action('ClientController@index')}}">Клиенты</a></li>
                <li><a href="{{action('ClientController@create')}}">Завести нового клиента</a></li>
            </ul>
        </div>

    </div>
</nav>


    <div class="container">
        <div style="margin-top: 80px;"></div>
        @yield('content')
    </div>
</body>
</html>