@extends('layouts.app')

@section('title', 'Новый клиент')

@section('content')
    <div class="row">
        <h2>Новый клиент</h2>
    </div>
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['action' => 'ClientController@store']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Имя') !!}
                {!! Form::text('name', '', ['class' => 'form-control']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('surname', 'Фамилия') !!}
                {!! Form::text('surname', '', ['class' => 'form-control']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('ssn', 'Идентификационный номер') !!}
                {!! Form::number('ssn', '', ['class' => 'form-control']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('gender', 'Пол') !!}
                {!! Form::select('gender', [ '1' => 'Мужчина', '2' => 'Женщина'],
                 '1', ['class' => 'form-control']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('birth', 'Дата рождения') !!}
                {!! Form::date('birth', \Carbon\Carbon::now()->subDay(), ['class' => 'form-control']); !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Сохранить') !!}
            </div>
        {!! Form::close() !!}
    </div>

@endsection