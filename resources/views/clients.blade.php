@extends('layouts.app')

@section('title', 'Клиенты банка')

@section('content')
    <div class="row">
        <h2>Клиенты банка</h2>
    </div>
    <div class="row">
        @if ($clients->count())
        <table class="table">
            @foreach($clients as $client)
                <tr>
                    <td>{{$client->surname}} {{$client->name}} ({{$client->ssn}})</td>
                    <td><a href="{{action('ClientController@show', ['id' => $client->id])}}">Смотреть</a></td>
                </tr>
            @endforeach
        </table>
        @else
            <h4>Нету</h4>
        @endif
    </div>

@endsection