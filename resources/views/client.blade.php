@extends('layouts.app')

@section('title', 'Клиент банка - '.$client->surname.' '.$client->name)

@section('content')
    <div class="row">
        <h2>Клиент банка - {{$client->surname}} {{$client->name}}</h2>
    </div>
    <div class="row">
        <table class="table">
            <tr><td>{{$client->surname}} {{$client->name}}</td></tr>
            <tr><td>{{$client->ssn}}</td></tr>
            <tr><td>{{$client->birth}}</td></tr>
            <tr><td>@if ($client->gender == 1) Мужчина @else Женщина @endif</td></tr>
        </table>
    </div>
    <div class="row">
        <h3>Добавить новый депозит</h3>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['action' => 'ClientController@addDeposit']) !!}
        <div class="form-group">
            {!! Form::label('balance', 'Сумма') !!}
            {!! Form::number('balance', '', ['class' => 'form-control']); !!}
        </div>
        <div class="form-group">
            {!! Form::label('percent', 'Процент') !!}
            {!! Form::number('percent', '', ['class' => 'form-control']); !!}
        </div>
        {!! Form::hidden('client_id', $client->id); !!}

        <div class="form-group">
            {!! Form::submit('Сохранить') !!}
        </div>
        {!! Form::close() !!}
    </div>
    <div class="row">
        <h3>Депозиты</h3>
        @if ($deposits)
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Сумма</th>
                    <th>Процент</th>
                </tr>
                @for ($i = 0; $i < count($deposits); $i++)
                    <tr>
                        <td>{{$deposits[$i]->balance}}</td>
                        <td>{{$deposits[$i]->percent}}</td>
                    </tr>
                @endfor
            </table>
        @else
            <h4>Нету</h4>
        @endif
    </div>

@endsection