@extends('layouts.app')

@section('title', 'Главная')

@section('content')
    <div class="row">
        @if ($depositsSum)
            <table class="table table-bordered table-hover">
                <tr>
                    <th>Возраст</th>
                    <th>Средняя сумма депозита</th>
                </tr>
                @for ($i = 0; $i < count($depositsSum); $i++)
                    <tr>
                        @if ($i == 0)
                            <td>I группа - От 18 до 25 лет</td>
                        @elseif ($i == 1)
                            <td>II группа - От 25 до 50 лет</td>
                        @else
                            <td>III группа - От 50 лет</td>
                        @endif
                        <td>{{$depositsSum[$i]}}</td>
                    </tr>
                @endfor
            </table>
        @endif
    </div>
    <div class="row">
        <div class="col-md-6">
            <h3>Убыток (суммы по депозитам)</h3>
            @if (count($lossProfit['loss']) > 0)
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Даты</th>
                        <th>Суммы</th>
                    </tr>
                    @for ($i = 0; $i < count($lossProfit['loss']); $i++)
                        <tr>
                            <td>{{$lossProfit['loss'][$i]->dt}}</td>
                            <td>{{$lossProfit['loss'][$i]->sum_money}}</td>
                        </tr>
                    @endfor
                </table>
            @else
                <h4>Нет данных</h4>
            @endif
        </div>
        <div class="col-md-6">
            <h3>Прибыль (комиссии)</h3>
            @if (count($lossProfit['profit']) > 0)
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Даты</th>
                        <th>Суммы</th>
                    </tr>
                    @for ($i = 0; $i < count($lossProfit['profit']); $i++)
                        <tr>
                            <td>{{$lossProfit['profit'][$i]->dt}}</td>
                            <td>{{$lossProfit['profit'][$i]->sum_money}}</td>
                        </tr>
                    @endfor
                </table>
            @else
                <h4>Нет данных</h4>
            @endif
        </div>
    </div>

@endsection
