<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['ssn', 'name', 'surname', 'gender', 'birth'];


    /*
     * Return all client's deposits
     */

    public function deposits()
    {
        return $this->hasMany(Deposit::class);
    }


    /*
     * Return all client's operations
     */

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }


}
