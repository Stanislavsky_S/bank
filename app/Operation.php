<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    /*
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = ['type', 'deposit_id', 'money'];


    /*
     * Return deposit
     */

    public function deposit()
    {
        return $this->belongsTo(Deposit::class);
    }

}
