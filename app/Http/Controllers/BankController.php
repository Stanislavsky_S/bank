<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Reports;

class BankController extends Controller
{

    protected $reports;

    public function __construct(Reports $reports)
    {
        $this->reports = $reports;
    }

    public function reportInfo()
    {
         return view('welcome', ['lossProfit' => $this->reports->lossProfit(),
            'depositsSum' => $this->reports->depositsSum()]);
    }
}
