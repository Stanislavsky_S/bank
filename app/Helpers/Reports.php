<?php

/*
 * Helper class for reports
 */

namespace App\Helpers;

use App\Operation;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Reports
{
    public function lossProfit()
    {
        $loss = Operation::select(DB::raw('date_format(created_at, "%Y-%m") as dt'), DB::raw('sum(money) as sum_money'))
            ->where('type', '=', 'deposit')
            ->groupBy('dt')
            ->orderBy('dt')
            ->get();
        $profit = Operation::select(DB::raw('date_format(created_at, "%Y-%m") as dt'), DB::raw('sum(money) as sum_money'))
            ->where('type', '=', 'commission')
            ->groupBy('dt')->orderBy('dt')->get();

        return ['loss' => $loss, 'profit' => $profit];
    }

    public function depositsSum()
    {
        $result = [];
        foreach ([[18, 25], [25,50], [50]] as $item) {
            if (count($item) > 1) {
                $firstDate = $this->getBirthData($item[0]);
                $secondDate = $this->getBirthData($item[1]);
                $people = DB::table('deposits')->leftJoin('clients', 'clients.id', '=', 'deposits.client_id')
                    ->whereDate('birth', '<=', $firstDate)
                    ->whereDate('birth', '>', $secondDate)->count();
                if ($people) {
                    $sum = DB::table('deposits')->leftJoin('clients', 'clients.id', '=', 'deposits.client_id')
                        ->whereDate('birth', '<=', $firstDate)
                        ->whereDate('birth', '>', $secondDate)->sum('balance');
                    array_push($result, $sum/$people);
                }
                else {
                    array_push($result, 0);
                }
            }
            else {
                $firstDate = $this->getBirthData($item[0]);
                $people = DB::table('deposits')->leftJoin('clients', 'clients.id', '=', 'deposits.client_id')
                    ->whereDate('birth', '<=', $firstDate)->count();
                if ($people) {
                    $sum = DB::table('deposits')->leftJoin('clients', 'clients.id', '=', 'deposits.client_id')
                        ->whereDate('birth', '<=', $firstDate)->sum('balance');
                    array_push($result, $sum/$people);
                }
                else {
                    array_push($result, 0);
                }
            }
        }

        return $result;
    }

    protected function getBirthData($age)
    {
        $date = new Carbon();
        $date->subYears($age);
        return $date->format('Y-m-d');
    }
}