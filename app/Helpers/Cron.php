<?php

/*
 * Helper class for calculate deposit percents and commissions
 */

namespace App\Helpers;

use Carbon\Carbon;
use App\Deposit;
use App\Operation;

class Cron
{
    public static function calculate()
    {
        $dt = Carbon::now();

        self::percentCalc($dt);
        if ($dt->day == 1) {
            self::commissionCalc($dt);
        }

        return 'Done';
    }

    protected static function percentCalc($dt)
    {
        if ($dt->day >= 28) {
            $checkLastDay = (new Carbon())->endOfMonth();
            if ($dt->day == $checkLastDay->day and ($dt->month == 2 or $dt->day == 30)) {
                $deps = Deposit::whereDay('created_at', '>=', $dt->day)
                    ->whereDate('created_at', '<>', $dt->format('Y-m-d'))
                    ->where('balance', '>', 0)
                    ->get();
            }
            else {
                $deps = Deposit::whereDay('created_at', $dt->day)
                    ->whereDate('created_at', '<>', $dt->format('Y-m-d'))
                    ->where('balance', '>', 0)
                    ->get();
            }
        }
        else {
            $deps = Deposit::whereDay('created_at', $dt->day)
                ->whereDate('created_at', '<>', $dt->format('Y-m-d'))
                ->where('balance', '>', 0)
                ->get();
        }

        self::updateDeps($deps);
    }

    protected static function updateDeps($deps)
    {
        if ($deps->count() > 0) {
            foreach ($deps as $dep) {
                $profit = $dep->balance * ($dep->percent / 100);
                $dep->balance += $profit;
                $dep->save();

                $operation = new Operation();
                $operation->type = 'deposit';
                $operation->deposit_id = $dep->id;
                $operation->money = $profit;
                $operation->save();
            }
        }
    }

    protected static function commissionCalc($dt)
    {
        $deps = Deposit::whereDate('created_at', '<>', $dt->format('Y-m-d'))
            ->where('balance', '>', 0)
            ->get();

        if ($deps->count() > 0) {
            foreach ($deps as $dep) {
                $commissionPercent = 1;
                if ($dep->created_at->month == $dt->month - 1 and $dep->created_at->year == $dt->year) {
                    $lastDateMonth = $dep->created_at->copy()->endOfMonth();
                    $commissionPercent = ($dep->created_at->diffInDays($lastDateMonth) + 1)/$lastDateMonth->day;
                }

                if ($dep->balance < 1000) {
                    $commission = $dep->balance * 0.05;
                    if ($commission < 50) {
                        $commission = 50;
                    }
                    $commission *= $commissionPercent;
                }
                else if ($dep->balance >= 1000 and $dep->balance < 10000) {
                    $commission = $dep->balance * 0.06;
                    $commission *= $commissionPercent;
                }
                else {
                    $commission = $dep->balance * 0.07;
                    if ($commission > 5000) {
                        $commission = 5000;
                    }
                    $commission *= $commissionPercent;
                }
                $dep->balance -= $commission;
                $dep->save();

                $operation = new Operation();
                $operation->type = 'commission';
                $operation->deposit_id = $dep->id;
                $operation->money = $commission;
                $operation->save();
            }
        }
    }
}