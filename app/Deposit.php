<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    /*
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = ['client_id', 'balance', 'percent'];
    protected $dates = ['created_at'];


    /*
     * Return all deposit operations
     */

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }



    /*
     * Return owner deposits
     */

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
